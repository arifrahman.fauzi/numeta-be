<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = "table_teams";
    protected $primaryKey = "team_id";
}
