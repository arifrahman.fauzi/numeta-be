<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tourney extends Model
{
    protected $table = 'table_tourney';
    protected $primaryKey = 'tourney_id';
}
