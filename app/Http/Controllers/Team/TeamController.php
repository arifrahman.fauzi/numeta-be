<?php

namespace App\Http\Controllers\Team;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Team;
use App\User;
use Auth;

class TeamController extends Controller
{
    public function index(){
        $team = Team::all();

        $response = ['status' => 'success', 'messsage' => 'Get All Team', 'data' => $team];

        return response($response, 200);
    }
    public function create_team(Request $req){

        $team = new Team;
        //$id = Auth::user();
        $id = auth('api')->user()->id;
        //dd($id);
        $team->nama_team = $req->nama_team;
        $team->captain_team = $id;
        $team->status_del = $req->status_del;
        $team->save();
        $response = ['team' => $team, 'message' => 'team has been created'];
        return response($response, 200);
        


    }
    public function update_team($id, Request $req){
        $team = Team::find($id);

        $team->nama_team = $req->nama_team;
        $team->status_del = $req->status_del;
        $team->save();

        $response = ['status' => 'success', 'message' => 'Team has been updated', 'data' => $team];
        return response($response, 200);
    }
    public function delete_team($id){
        $team = Team::find($id);
        $team->status_del = 1;
        $team->save();

        return response(['status' => 'success', 'message' => 'Team has been delete'], 204);
    }
    
}
