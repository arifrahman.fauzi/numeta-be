<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>
<img src="./logo-tidur.png" width="300">
<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Numeta Back-End

#### di clone dulu, 

#### setting database di file .env

```bash
composer install
php artisan key:generate
php artisan migrate
```
### untuk melihat list _endpoint_ akses   /routes
### Jangan lupa versi php nya di atas 7.1 

#### Lihat Routing List http://35.185.189.135/numeta-backend/public/routes

## Auth Bearer Token
#### Header
`Accept : Application/json`

`Authorization : Bearer`

| Method | EndPoint | parameter | Keterangan|
| ------ | ------ |--------|-----|
| Post| http://35.185.189.135/numeta-backend/public/api/register |username, email, password, password_confirmation| |
| Post| http://35.185.189.135/numeta-backend/public/api/login | email, password| |
| Get | http://35.185.189.135/numeta-backend/public/api/user/2 | langsung isi id 2 | |
| Post| http://35.185.189.135/numeta-be/public/api/team | nama_team, status_del(0)| |
| Get | http://35.185.189.135/numeta-be/public/api/profile/2 | langsung id user| |
| Get | http://35.185.189.135/numeta-be/public/api/listuser | | |
| Get | http://35.185.189.135/numeta-be/public/api/getProPlayers| | |
| Get | http://35.185.189.135/numeta-be/public/api/team| Get all team | |
| Put | http://35.185.189.135/numeta-be/public/api/team/1 | nama_team, status_del | update team|

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
