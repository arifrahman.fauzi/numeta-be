<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile', function (Blueprint $table) {
            $table->bigIncrements('id_profile');
            $table->string('photo_link')->nullable();
            $table->char('bio')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('gender')->nullable();
            $table->string('team')->nullable();
            $table->string('region')->nullable();
            $table->string('school')->nullable();
            $table->string('steam_id')->nullable();
            $table->integer('followers')->nullable();
            $table->integer('connections')->nullable();
            $table->string('rep_point')->nullable();
            $table->string('photos_link')->nullable();
            $table->integer('video')->nullable();
            $table->string('steam_link')->nullable();
            $table->smallInteger('status_del')->nullabel();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile');
    }
}
